package me.zv.fitss;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by MM on 13.05.14.
 */
public class DbUpdateHelper {

    private static final int MUSCLES = 1;
    private static final int EXERCISES = 3;

    private static final UriMatcher uriMatcher;

    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(ProviderContract.Muscle.CONTENT_URI.getAuthority(), ProviderContract.Muscle.CONTENT_URI.getPath(), MUSCLES);
        uriMatcher.addURI(ProviderContract.Exercise.CONTENT_URI.getAuthority(), ProviderContract.Exercise.CONTENT_URI.getPath(), EXERCISES);
    }

    public static void update(JSONArray data, Uri uri, ContentResolver resolver){
        ContentValues cv;
        GetCVCallback getCVCallback;

        switch(uriMatcher.match(uri)){
            case EXERCISES:
                getCVCallback = new GetCVExercise();
                break;
            case MUSCLES:
                getCVCallback = new GetCVMuscle();
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        for(int i = 0; i < data.length(); i++){
            try {
                cv = getCVCallback.getCV(data.getJSONObject(i));
                if(cv != null){
                    Uri newUri = resolver.insert(uri, cv);
                    Log.wtf("debug", newUri.toString());
                }

            } catch (JSONException e){
                //TODO: отправить отчет об ошибке{
                e.printStackTrace();
            }

        }

    }

    private static interface GetCVCallback {
        ContentValues getCV(JSONObject json);
    }

    private static class GetCVExercise implements GetCVCallback {

        @Override
        public ContentValues getCV(JSONObject json) {
            long id;
            String title;
            long muscleId;
            try {
                id =  json.getLong("id");
                title = json.getString("title");
                muscleId = json.getLong("muscle");
            } catch (JSONException e) {
                //TODO: отправить отчет об ошибке
                e.printStackTrace();
                return null;
            }

            if((id != 0) && (title != null) && (muscleId != 0)){
                ContentValues cv = new ContentValues();
                cv.put(ProviderContract.Exercise._ID, id);
                cv.put(ProviderContract.Exercise.COLUMN_TITLE, title);
                cv.put(ProviderContract.Exercise.COLUMN_MUSCLE_ID, muscleId);
                return cv;
            }

            return null;
        }
    }

    private static class GetCVMuscle implements GetCVCallback {

        @Override
        public ContentValues getCV(JSONObject json) {
            long id;
            String title;
            try {
                id =  json.getLong("id");
                title = json.getString("title");
            } catch (JSONException e) {
                //TODO: отправить отчет об ошибке
                e.printStackTrace();
                return null;
            }

            if((id != 0) && (title != null)){
                ContentValues cv = new ContentValues();
                cv.put(ProviderContract.Muscle._ID, id);
                cv.put(ProviderContract.Muscle.COLUMN_TITLE, title);
                return cv;
            }

            return null;
        }
    }


}
