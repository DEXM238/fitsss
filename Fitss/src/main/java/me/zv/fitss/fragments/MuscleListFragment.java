package me.zv.fitss.fragments;


import android.app.ListFragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import me.zv.fitss.ProviderContract;
import me.zv.fitss.activities.ExerciseListActivity;

/**
 * Created by MM on 21.05.14.
 */
public class MuscleListFragment extends ListFragment {

    private static SimpleCursorAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Cursor cursor = getActivity().getContentResolver().query(ProviderContract.Muscle.CONTENT_URI, null, null, null, null);

        String[] from = new String[]{ProviderContract.Muscle.COLUMN_TITLE};
        int[] to = new int[] {android.R.id.text1};

        adapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1, cursor, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        startActivity(new Intent(getActivity(), ExerciseListActivity.class));
    }



}
