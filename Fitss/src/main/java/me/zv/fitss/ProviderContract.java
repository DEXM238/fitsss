package me.zv.fitss;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Описание схемы данных
 */
public final class ProviderContract {
    public static final String AUTHORITY = "me.zv.fitss.provider";

    //This class cannot be instantiated
    private ProviderContract(){}

    public static final class Muscle implements BaseColumns {

        //This class cannot be instantiated
        private Muscle(){}

        public static final String TABLE_NAME = "muscle";
        private static final String SCHEME = "content://";
        private static final String PATH_MUSCLES = "/muscle";
        private static final String PATH_MUSCLE_ID = "/muscle/";
        public static final int MUSCLE_ID_PATH_POSITION = 1;
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_MUSCLES);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_MUSCLE_ID);
        public static final Uri CONTENT_ID_URI_PATTERN = Uri.parse(SCHEME + AUTHORITY + PATH_MUSCLE_ID + "/#");
        public static final String CONTENT_TYPE = "vnd.zv.cursor.dir/vnd.zv.muscle";
        public static final String CONTENT_ITEM_TYPE = "vnd.zv.cursor.item/vnd.zv.muscle";
        public static final String DEFAULT_SORT_ORDER = _ID + " ASC";
        /*
         * Column definitions
         */

        /**
         * Название
         * <P>Type: TEXT</P>
         */
        public static final String COLUMN_TITLE = "title";
    }

    public static final class Exercise implements BaseColumns {

        private Exercise(){}

        public static final String TABLE_NAME = "exercise";
        private static final String SCHEME = "content://";
        private static final String PATH_EXERCISES = "/exercise";
        private static final String PATH_EXERCISE_ID = "/exercise/";
        public static final int EXERCISE_ID_PATH_POSITION = 1;
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_EXERCISES);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_EXERCISE_ID);
        public static final Uri CONTENT_ID_URI_PATTERN = Uri.parse(SCHEME + AUTHORITY + PATH_EXERCISE_ID + "/#");
        public static final String CONTENT_TYPE = "vnd.zv.cursor.dir/vnd.zv.exercise";
        public static final String CONTENT_ITEM_TYPE = "vnd.zv.cursor.item/vnd.zv.exercise";
        public static final String DEFAULT_SORT_ORDER = _ID + " ASC";
        /*
         * Column definitions
         */

        /**
         * Название
         * <P>Type: TEXT</P>
         */
        public static final String COLUMN_TITLE = "title";

        /**
         * Основная мышечная группа
         * <P>Type: INTEGER</P>
         */
        public static final String COLUMN_MUSCLE_ID = "muscle_id";

    }

    public static final class Set implements BaseColumns {

        private Set(){}

        public static final String TABLE_NAME = "'set'";
        private static final String SCHEME = "content://";
        private static final String PATH_SETS = "/set";
        private static final String PATH_SET_ID = "/set/";
        public static final int SET_ID_PATH_POSITION = 1;
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SETS);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_SET_ID);
        public static final Uri CONTENT_ID_URI_PATTERN = Uri.parse(SCHEME + AUTHORITY + PATH_SET_ID + "/#");
        public static final String CONTENT_TYPE = "vnd.zv.cursor.dir/vnd.zv.set";
        public static final String CONTENT_ITEM_TYPE = "vnd.zv.cursor.item/vnd.zv.set";
        public static final String DEFAULT_SORT_ORDER = _ID + " ASC";
        /*
         * Column definitions
         */

        /**
         * Идентификатор тренировки
         * <P>Type: INTEGER</P>
         */
        public static final String COLUMN_WORKOUT_ID = "workout_id";

        /**
         * Номер подхода
         * <P>Type: INTEGER</P>
         */
        public static final String COLUMN_NUMBER = "number";

        /**
         * Вес
         * <P>Type: INTEGER</P>
         */
        public static final String COLUMN_WEIGHT = "weight";

        /**
         * Число повторений
         * <P>Type: INTEGER</P>
         */
        public static final String COLUMN_REPEAT = "repeat";

    }

    public static final class Workout implements BaseColumns {

        private Workout(){}

        public static final String TABLE_NAME = "workout";
        private static final String SCHEME = "content://";
        private static final String PATH_WORKOUTS = "/workout";
        private static final String PATH_WORKOUT_ID = "/workout/";
        public static final int WORKOUT_ID_PATH_POSITION = 1;
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_WORKOUTS);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_WORKOUT_ID);
        public static final Uri CONTENT_ID_URI_PATTERN = Uri.parse(SCHEME + AUTHORITY + PATH_WORKOUT_ID + "/#");
        public static final String CONTENT_TYPE = "vnd.zv.cursor.dir/vnd.zv.workout";
        public static final String CONTENT_ITEM_TYPE = "vnd.zv.cursor.item/vnd.zv.workout";
        public static final String DEFAULT_SORT_ORDER = _ID + " ASC";
        /*
         * Column definitions
         */

        /**
         * Идентификатор упражнения
         * <P>Type: INTEGER</P>
         */
        public static final String COLUMN_EXERCISE_ID = "exercise_id";

        /**
         * Комментарий
         * <P>Type: TEXT</P>
         */
        public static final String COLUMN_COMMENT = "comment";

        /**
         * Дата
         * <P>Type: DATETIME</P>
         */
        public static final String COLUMN_DATE = "date";
    }
}
