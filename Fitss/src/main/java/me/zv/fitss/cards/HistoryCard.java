package me.zv.fitss.cards;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;
import me.zv.fitss.R;
import me.zv.fitss.entities.Set;

/**
 * Карточка для отображения истории прошлых тренировок
 */
public class HistoryCard extends CardWithList {
    private TextView mNumber;
    private TextView mWeight;
    private TextView mRepeat;


    public HistoryCard(Context context) {
        super(context);
    }

    /**
     * Добавляет на карточку данные подхода
     *
     * @param set
     */
    public void addSet(Set set) {
        mLinearListAdapter.add(new SetObject(this, set));
    }

    /**
     * Устанавливает дату, которая отображается в заголовке
     *
     * @param date
     */
    public void setDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM yyyy");
        getCardHeader().setTitle(sdf.format(date));
    }

    @Override
    protected CardHeader initCardHeader() {
        mCardHeader = new CardHeader(getContext());
        mCardHeader.setTitle(getContext().getString(R.string.history));
        return mCardHeader;
    }

    @Override
    protected void initCard() {

    }

    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> list = new ArrayList<ListObject>();
        return list;
    }

    @Override
    public View setupChildView(int i, ListObject listObject, View view, ViewGroup viewGroup) {

        mNumber = (TextView) view.findViewById(R.id.numberTextView);
        mWeight = (TextView) view.findViewById(R.id.weightTextView);
        mRepeat = (TextView) view.findViewById(R.id.repeatTextView);

        SetObject setObject = (SetObject) listObject;
        Set set = setObject.getSet();

        mNumber.setText(Integer.toString(set.getNumber()));
        mWeight.setText(Integer.toString(set.getWeight()));
        mRepeat.setText(Integer.toString(set.getRepeat()));

        return view;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.history_set_item;
    }

    private class SetObject extends DefaultListObject {
        Set mSet;

        public SetObject(Card parentCard, Set set) {
            super(parentCard);
            mSet = set;
        }

        public Set getSet() {
            return mSet;
        }
    }
}
