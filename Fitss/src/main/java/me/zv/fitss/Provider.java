package me.zv.fitss;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;


/**
 * Стандартный Content Provider
 */
public class Provider extends ContentProvider {
    /*
     * Constants used by the Uri matcher to choose an action based on the pattern
     * of the incoming URI
     */
    private static final int MUSCLES = 1;
    private static final int MUSCLE_ID = 2;

    private static final int EXERCISES = 3;
    private static final int EXERCISE_ID = 4;

    private static final int WORKOUTS = 5;
    private static final int WORKOUT_ID = 6;

    private static final int SETS = 7;
    private static final int SET_ID = 8;

    private static final String DB_NAME = "fitss";
    private static final int DB_VERSION = 4;

    private static final UriMatcher uriMatcher;

    private SQLiteDatabase mDB;
    private DBHelper mOpenHelper;

    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(ProviderContract.Muscle.CONTENT_URI.getAuthority(), ProviderContract.Muscle.CONTENT_URI.getPath(), MUSCLES);
        uriMatcher.addURI(ProviderContract.Exercise.CONTENT_URI.getAuthority(), ProviderContract.Exercise.CONTENT_URI.getPath(), EXERCISES);
        uriMatcher.addURI(ProviderContract.Workout.CONTENT_URI.getAuthority(), ProviderContract.Workout.CONTENT_URI.getPath(), WORKOUTS);
        uriMatcher.addURI(ProviderContract.Set.CONTENT_URI.getAuthority(), ProviderContract.Set.CONTENT_URI.getPath(), SETS);
        uriMatcher.addURI(ProviderContract.AUTHORITY, "exercise/#", EXERCISE_ID);
        uriMatcher.addURI(ProviderContract.AUTHORITY, "muscle/#", MUSCLE_ID);
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        String orderBy;
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch(uriMatcher.match(uri)){
            case MUSCLES:
                qb.setTables(ProviderContract.Muscle.TABLE_NAME);
                orderBy = ProviderContract.Muscle.DEFAULT_SORT_ORDER;
                break;
            case EXERCISES:
                qb.setTables(ProviderContract.Exercise.TABLE_NAME);
                orderBy = ProviderContract.Exercise.DEFAULT_SORT_ORDER;
                break;
            case WORKOUTS:
                qb.setTables(ProviderContract.Workout.TABLE_NAME);
                orderBy = ProviderContract.Workout.DEFAULT_SORT_ORDER;
                break;
            case SETS:
                qb.setTables(ProviderContract.Set.TABLE_NAME);
                orderBy = ProviderContract.Set.DEFAULT_SORT_ORDER;
                break;
            case EXERCISE_ID:
                qb.setTables(ProviderContract.Exercise.TABLE_NAME);
                qb.appendWhere(ProviderContract.Exercise._ID + " = " + uri.getPathSegments().get(ProviderContract.Exercise.EXERCISE_ID_PATH_POSITION));
                orderBy = ProviderContract.Exercise.DEFAULT_SORT_ORDER;
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }

        if(!TextUtils.isEmpty(sortOrder)){
            orderBy = sortOrder;
        }

        mDB = mOpenHelper.getWritableDatabase();
        Cursor cursor = qb.query(mDB, projection, selection, selectionArgs, null, null, orderBy);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        //валидация
        if(values == null){
            throw new IllegalArgumentException("Values are empty");
        }

        long rowId;
        Uri noteUri = null;
        this.mDB = mOpenHelper.getWritableDatabase();

        switch (uriMatcher.match(uri)){
            case MUSCLES:
                rowId = this.mDB.insertWithOnConflict(ProviderContract.Muscle.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if(rowId > 0){
                    noteUri = ContentUris.withAppendedId(ProviderContract.Muscle.CONTENT_ID_URI_BASE, rowId);
                }
                break;
            case EXERCISES:
                rowId = this.mDB.insertWithOnConflict(ProviderContract.Exercise.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if(rowId > 0){
                    noteUri = ContentUris.withAppendedId(ProviderContract.Exercise.CONTENT_ID_URI_BASE, rowId);
                }
                break;
            case SETS:
                rowId = this.mDB.insertWithOnConflict(ProviderContract.Set.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if(rowId > 0){
                    noteUri = ContentUris.withAppendedId(ProviderContract.Set.CONTENT_ID_URI_BASE, rowId);
                }
                break;
            case WORKOUTS:
                rowId = this.mDB.insertWithOnConflict(ProviderContract.Workout.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if(rowId > 0){
                    noteUri = ContentUris.withAppendedId(ProviderContract.Workout.CONTENT_ID_URI_BASE, rowId);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        //оповестить об изменении данных
        if(noteUri != null){
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }

        throw new SQLException("Failed to insert row " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    protected static final class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + ProviderContract.Muscle.TABLE_NAME + " ("
                    + ProviderContract.Muscle._ID + " INTEGER PRIMARY KEY,"
                    + ProviderContract.Muscle.COLUMN_TITLE + " TEXT"
                    + ");"
            );

            db.execSQL("CREATE TABLE " + ProviderContract.Exercise.TABLE_NAME + " ("
                    + ProviderContract.Exercise._ID + " INTEGER PRIMARY KEY,"
                    + ProviderContract.Exercise.COLUMN_TITLE + " TEXT,"
                    + ProviderContract.Exercise.COLUMN_MUSCLE_ID + " INTEGER,"
                    + " FOREIGN KEY (" + ProviderContract.Exercise.COLUMN_MUSCLE_ID + ") REFERENCES "
                        + ProviderContract.Muscle.TABLE_NAME + " (" + ProviderContract.Muscle._ID + "));"

            );

            db.execSQL("CREATE TABLE " + ProviderContract.Workout.TABLE_NAME + " ("
                    + ProviderContract.Workout._ID + " INTEGER PRIMARY KEY,"
                    + ProviderContract.Workout.COLUMN_COMMENT + " TEXT,"
                    + ProviderContract.Workout.COLUMN_DATE + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                    + ProviderContract.Workout.COLUMN_EXERCISE_ID + " INTEGER,"
                    + " FOREIGN KEY (" + ProviderContract.Workout.COLUMN_EXERCISE_ID + ") REFERENCES "
                        + ProviderContract.Exercise.TABLE_NAME + " (" + ProviderContract.Exercise._ID + "));"
            );

            db.execSQL("CREATE TABLE " + ProviderContract.Set.TABLE_NAME + " ("
                    + ProviderContract.Set._ID + " INTEGER PRIMARY KEY,"
                    + ProviderContract.Set.COLUMN_NUMBER + " INTEGER,"
                    + ProviderContract.Set.COLUMN_WEIGHT + " INTEGER,"
                    + ProviderContract.Set.COLUMN_REPEAT + " INTEGER,"
                    + ProviderContract.Set.COLUMN_WORKOUT_ID + " INTEGER,"
                    + " FOREIGN KEY (" + ProviderContract.Set.COLUMN_WORKOUT_ID + ") REFERENCES "
                        + ProviderContract.Workout.TABLE_NAME + " (" + ProviderContract.Workout._ID + "));"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + ProviderContract.Muscle.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + ProviderContract.Exercise.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + ProviderContract.Workout.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + ProviderContract.Set.TABLE_NAME);
            onCreate(db);
        }
    }
}
