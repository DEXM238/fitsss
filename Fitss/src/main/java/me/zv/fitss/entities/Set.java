package me.zv.fitss.entities;

import android.content.ContentValues;

import me.zv.fitss.ProviderContract;

/**
 * Подход
 */
public class Set {
    protected int mNumber;
    protected int mWeight;
    protected int mRepeat;

    public Set(int mNumber, int mWeight, int mRepeat) {
        this.mWeight = mWeight;
        this.mNumber = mNumber;
        this.mRepeat = mRepeat;
    }

    public Set(int mNumber, String mWeight, String mRepeat) {
        this.mWeight = Integer.parseInt(mWeight);
        this.mNumber = mNumber;
        this.mRepeat = Integer.parseInt(mRepeat);
    }

    public int getNumber() {
        return mNumber;
    }

    public int getWeight() {
        return mWeight;
    }

    public int getRepeat() {
        return mRepeat;
    }

    /**
     * Возвращает заполенные ContentValues для подхода
     *
     * @param workoutId идентификатор теринировки
     * @return заполненые CV
     */
    public ContentValues getCV(long workoutId){
        ContentValues cv = new ContentValues();
        cv.put(ProviderContract.Set.COLUMN_NUMBER, mNumber);
        cv.put(ProviderContract.Set.COLUMN_WEIGHT, mWeight);
        cv.put(ProviderContract.Set.COLUMN_REPEAT, mRepeat);
        cv.put(ProviderContract.Set.COLUMN_WORKOUT_ID, workoutId);
        return cv;
    }
}
