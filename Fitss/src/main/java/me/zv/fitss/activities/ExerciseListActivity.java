package me.zv.fitss.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import me.zv.fitss.ProviderContract;

/**
 * Created by MM on 06.06.14.
 */
public class ExerciseListActivity extends ListActivity {

    private static SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Cursor c  = getContentResolver().query(ProviderContract.Exercise.CONTENT_URI, null, null, null, null);
        String[] from = new String[]{ProviderContract.Exercise.COLUMN_TITLE};
        int[] to = new int[] {android.R.id.text1};

        adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, c, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(this, DoExerciseActivity.class);
        intent.putExtra(DoExerciseActivity.EXTRA_EXERCISE_ID, id);
        startActivity(intent);
    }

}
