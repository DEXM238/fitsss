package me.zv.fitss.activities;

import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.Date;

import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardView;
import me.zv.fitss.ProviderContract;
import me.zv.fitss.R;
import me.zv.fitss.SetCard;
import me.zv.fitss.cards.HistoryCard;
import me.zv.fitss.db.ProviderHelper;
import me.zv.fitss.entities.Set;


/**
 * Для записи результатов тренировки
 */
public class DoExerciseActivity extends Activity {
    public static final String EXTRA_EXERCISE_ID = "exercise_id";

    protected static final String[] PROJECTION = new String[]{
            ProviderContract.Exercise._ID,
            ProviderContract.Muscle.COLUMN_TITLE,
    };

    protected static final int COLUMN_TITLE_INDEX = 1;

    protected static SetCard sets;
    protected static HistoryCard history;
    protected static CardView setsView;
    protected static CardView historyView;
    protected static CardHeader setsHeader;

    protected static long mExerciseId;

    protected void initUI(){
        sets = new SetCard(this);
        setsHeader = new CardHeader(this);
        setsHeader.setTitle(getString(R.string.workout));
        sets.addCardHeader(setsHeader);
        setsView = (CardView)findViewById(R.id.sets);
        setsView.setCard(sets);

        history = new HistoryCard(this);
        history.init();
        historyView = (CardView)findViewById(R.id.history);
        historyView.setCard(history);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_exercise);
        initUI();

        Bundle extras = getIntent().getExtras();
        mExerciseId = extras.getLong(EXTRA_EXERCISE_ID);
        if(mExerciseId == 0){
            throw new RuntimeException("Нужно передать id приложения");
        }

        Cursor lastWorkouts = ProviderHelper.getWorkouts(this, mExerciseId);
        lastWorkouts.moveToFirst();

        long timestamp = Long.parseLong(lastWorkouts.getString(1));
        long workoutId = lastWorkouts.getLong(0);

        Cursor sets = ProviderHelper.getWorkoutSets(this, workoutId);
        while(sets.moveToNext()){
            history.addSet(new Set(sets.getInt(1), sets.getInt(2), sets.getInt(3)));
        }
        sets.close();

        Date date = new Date(timestamp*1000);

        history.setDate(date);
        historyView.refreshCard(history);

        Uri uri = ContentUris.withAppendedId(ProviderContract.Exercise.CONTENT_ID_URI_BASE, mExerciseId);
        Cursor c = getContentResolver().query(uri, PROJECTION, null, null, null);
        c.moveToNext();

        setTitle(c.getString(COLUMN_TITLE_INDEX));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.do_exercise, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:
                ProviderHelper.addWorkout(this, sets.getSetsList(), mExerciseId);
                try{
                    File sd = Environment.getExternalStorageDirectory();
                    File data = Environment.getDataDirectory();
                    if(sd.canWrite()){
                        String currentDBPath = "//data//" + getPackageName() + "//databases//fitss";
                        String backupDBPath = "backupname.db";
                        File currentDB = new File(data, currentDBPath);
                        File backupDB = new File(sd, backupDBPath);
                        if (currentDB.exists()) {
                            Toast.makeText(this, "exist", Toast.LENGTH_SHORT).show();
                            FileChannel src = new FileInputStream(currentDB).getChannel();
                            FileChannel dst = new FileOutputStream(backupDB).getChannel();
                            dst.transferFrom(src, 0, src.size());
                            src.close();
                            dst.close();
                        }else {
                            Toast.makeText(this, "not exist(((", Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(this, "backup", Toast.LENGTH_SHORT).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
