package me.zv.fitss;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import me.zv.fitss.utils.Network;

/**
 * Настройки
 */
public class SettingFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener{

    private DownloadTask mDownloadTask;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        Preference refreshPref = findPreference("pref_key_refresh");
        refreshPref.setOnPreferenceClickListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if(!Network.isConnected(getActivity())){
            Toast.makeText(getActivity(), getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        }

        this.mDownloadTask = new DownloadTask();
        this.mDownloadTask.execute();
        return true;
    }

    private class DownloadTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {

            update("http://192.168.0.15/api/muscle", ProviderContract.Muscle.CONTENT_URI);
            update("http://192.168.0.15/api/exercise", ProviderContract.Exercise.CONTENT_URI);
            return null;
        }

        private void update(String url, Uri uri){
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            try {
                HttpResponse response = httpClient.execute(request);
                BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while((line = in.readLine()) != null){
                    stringBuilder.append(line);
                }

                try {
                    DbUpdateHelper.update(new JSONArray(stringBuilder.toString()), uri, getActivity().getContentResolver());
                } catch (JSONException e) {
                    //TODO: окно не удалось обновить
                    e.printStackTrace();
                }

            } catch (IOException e) {
                //TODO: окно не удалось обновить
                e.printStackTrace();
            }
        }
    }
}
