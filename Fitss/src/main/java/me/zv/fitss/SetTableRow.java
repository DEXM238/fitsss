package me.zv.fitss;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import me.zv.fitss.entities.Set;


/**
 * Created by MM on 19.06.14.
 */
public class SetTableRow extends TableRow {
    protected LayoutParams mRowParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    protected LayoutParams mTextViewParams = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);

    protected TextView mNumber;
    protected TextView mWeight;
    protected TextView mRepeat;

    public SetTableRow(Context context) {
        super(context);
        this.setLayoutParams(mRowParams);
        this.setWeightSum(3);

        mNumber = new TextView(context);
        mWeight = new TextView(context);
        mRepeat = new TextView(context);
        mNumber.setLayoutParams(mTextViewParams);
        mWeight.setLayoutParams(mTextViewParams);
        mRepeat.setLayoutParams(mTextViewParams);
        this.addView(mNumber);
        this.addView(mWeight);
        this.addView(mRepeat);
    }


    public void setData(Set set){
        mNumber.setText(Integer.toString(set.getNumber()));
        mWeight.setText(Integer.toString(set.getWeight()));
        mRepeat.setText(Integer.toString(set.getRepeat()));
    }
}
