package me.zv.fitss.db;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.zv.fitss.ProviderContract;
import me.zv.fitss.entities.Set;

/**
 * Методы для работы с Content Provider
 */
public class ProviderHelper {

    /**
     * Сохранение тренировки
     *
     * @param context
     * @param sets
     * @param exerciseId
     * @return
     */
    public static long addWorkout(Context context, List<Set> sets, long exerciseId){
        if(sets.isEmpty()){
            throw new IllegalArgumentException("Список подходов не должен быть пустым");
        }

        ContentResolver cr = context.getContentResolver();

        ContentValues workout = new ContentValues();
        workout.put(ProviderContract.Workout.COLUMN_EXERCISE_ID, exerciseId);
        workout.put(ProviderContract.Workout.COLUMN_DATE, new Date().getTime()/1000);

        Uri workoutUri = cr.insert(ProviderContract.Workout.CONTENT_URI, workout);
        long workoutId = ContentUris.parseId(workoutUri);
        if(workoutId == -1){
            throw new RuntimeException("Ошибка при добавлении тренировки в бд");
        }

        ArrayList<ContentProviderOperation> setAddList = new ArrayList<ContentProviderOperation>(sets.size());
        for(Set set : sets){
            setAddList.add(ContentProviderOperation.newInsert(ProviderContract.Set.CONTENT_URI).withValues(set.getCV(workoutId)).build());
        }
        try {
            cr.applyBatch(ProviderContract.AUTHORITY, setAddList);
        } catch (RemoteException e) {
            throw new RuntimeException("Ошибка при вставке подхода: " + e.getMessage());
        } catch (OperationApplicationException e) {
            throw new RuntimeException("Ошибка при вставке подхода: " + e.getMessage());
        }

        return workoutId;
    }

    /**
     * Получение всех тренировок для упражнения
     *
     * @param context
     * @param exerciseId
     * @return
     */
    public static Cursor getWorkouts(Context context, long exerciseId){
        ContentResolver cr = context.getContentResolver();
        String[] projection = {ProviderContract.Workout._ID, ProviderContract.Workout.COLUMN_DATE};
        String selection = ProviderContract.Workout.COLUMN_EXERCISE_ID + " = ?";
        String[] selectionArgs = {Long.toString(exerciseId)};
        String sortOrder = ProviderContract.Workout.COLUMN_DATE + " DESC";
        return cr.query(ProviderContract.Workout.CONTENT_URI, projection, selection, selectionArgs, sortOrder);
    }

    /**
     * Получение подходов для тренировки
     *
     * @param context
     * @param workoutId
     * @return
     */
    public static Cursor getWorkoutSets(Context context, long workoutId){
        ContentResolver cr = context.getContentResolver();
        String[] projection = {ProviderContract.Set._ID, ProviderContract.Set.COLUMN_NUMBER, ProviderContract.Set.COLUMN_WEIGHT, ProviderContract.Set.COLUMN_REPEAT};
        String selection = ProviderContract.Set.COLUMN_WORKOUT_ID + " = ?";
        String[] selectionArgs = {Long.toString(workoutId)};
        String sortOrder = ProviderContract.Set.COLUMN_NUMBER + " ASC";
        return cr.query(ProviderContract.Set.CONTENT_URI, projection, selection, selectionArgs, sortOrder);
    }
}
