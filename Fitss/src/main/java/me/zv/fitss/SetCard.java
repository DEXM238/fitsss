package me.zv.fitss;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import me.zv.fitss.entities.Set;

/**
 * Карточка для записи подходов по упражнению
 */
public class SetCard extends Card implements View.OnClickListener, TextWatcher{

    private TableLayout mSetsTable;
    private Button mAddSetButton;
    private EditText mWeightEditText;
    private EditText mRepeatEditText;
    private List<Set> mSetsList;
    /**
     * Счетчик подходов
     */
    private int number = 0;

    public SetCard(Context context) {
        super(context);
        this.setInnerLayout(R.layout.set);
        mSetsList = new ArrayList<Set>(10);
    }

    public List<Set> getSetsList(){
        return mSetsList;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view){
        mSetsTable = (TableLayout)view.findViewById(R.id.setsTableLayout);
        mAddSetButton = (Button)view.findViewById(R.id.addSetButton);
        mWeightEditText = (EditText)view.findViewById(R.id.weightEditText);
        mRepeatEditText = (EditText)view.findViewById(R.id.repeatEditText);

        mAddSetButton.setOnClickListener(this);
        mWeightEditText.addTextChangedListener(this);
        mRepeatEditText.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.addSetButton:
                if(isEditTextsNotEmpty()){
                    addRow();
                }
                break;
            default:
               throw new IllegalArgumentException();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        //кнопка доступна только, если заполнены оба поля
        mAddSetButton.setEnabled(isEditTextsNotEmpty());
    }

    private boolean isEditTextsNotEmpty(){
        return (mWeightEditText.getText().length()  > 0) && (mRepeatEditText.getText().length() > 0);
    }

    /**
     * Добавляет новую строку в таблицу с текущими данными
     */
    private void addRow(){
        number++;
        SetTableRow r = new SetTableRow(getContext());
        Set set = new Set(number, mWeightEditText.getText().toString(), mRepeatEditText.getText().toString());
        mSetsList.add(set);
        r.setData(set);
        mSetsTable.addView(r);
        mWeightEditText.setText("");
        mRepeatEditText.setText("");
    }
}
